FROM ubuntu:18.04

RUN apt-get update && \
    apt-get -y install gcc libc6-dev zlib1g-dev curl bash  && \
    rm -rf /var/lib/apt/lists/*

# Download GraalVM
RUN curl -4 -L https://github.com/oracle/graal/releases/download/vm-19.2.0/graalvm-ce-linux-amd64-19.2.0.tar.gz -o /tmp/graalvm-ce-linux-amd64-19.2.0.tar.gz


# Untar and move the files we need:
RUN tar -zxvf /tmp/graalvm-ce-linux-amd64-19.2.0.tar.gz -C /tmp && \
    mv /tmp/graalvm-ce-19.2.0 /usr/lib/graalvm && \
    /usr/lib/graalvm/bin/gu install native-image

ENV JAVA_HOME=/usr/lib/graalvm \
    PATH=$PATH:$JAVA_HOME/bin

